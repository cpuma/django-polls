# Django2.1-Tutorial
https://docs.djangoproject.com/ja/2.1/intro/tutorial01/

Runnning Locally
---
Make sure you have [Docker Compose](https://docs.docker.com/compose/install/).

```sh
$ cp .env.example .env
$ docker-compose up
```

Your app should now be running on http://0.0.0.0:8000/polls/
